package storage

import "errors"

type Book struct {
	Title  string
	Author string
	Year   int
	Text   string
}

type Storage interface {
	GetBook(id int) (*Book, error)
	AddBook(id int, book *Book) error
}

type MapStorage struct {
	books map[int]*Book
}

func NewMapStorage() *MapStorage {
	return &MapStorage{books: make(map[int]*Book)}
}

func (m *MapStorage) AddBook(id int, book *Book) error {
	if _, ok := m.books[id]; ok {
		return errors.New("collision")
	}
	m.books[id] = book
	return nil
}

func (m *MapStorage) GetBook(id int) (*Book, error) {
	if _, ok := m.books[id]; !ok {
		return &Book{}, errors.New("element does not exist")
	}
	return m.books[id], nil
}

type ListStorage struct {
	books     []*Book
	idToIndex map[int]int
}

func NewListStorage() *ListStorage {
	return &ListStorage{idToIndex: make(map[int]int)}
}

func (l *ListStorage) AddBook(id int, book *Book) error {
	if _, ok := l.idToIndex[id]; ok {
		return errors.New("collision")
	}
	l.idToIndex[id] = len(l.books)
	l.books = append(l.books, book)
	return nil
}

func (l *ListStorage) GetBook(id int) (*Book, error) {
	return l.books[l.idToIndex[id]], nil
}
