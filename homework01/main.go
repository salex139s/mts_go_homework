package main

import (
	"fmt"
	"homework01/library"
	"homework01/storage"
)

func hash(str *string) int {
	const module = 1610612741
	const multiplier = 7
	res := 0
	for _, s := range *str {
		res = (res*multiplier + int(s)) % module
	}
	return res
}

func hash2(str *string) int {
	const module = 402653189
	const multiplier = 11
	res := 0
	for _, s := range *str {
		res = (res*multiplier + int(s)) % module
	}
	return res
}

func main() {
	books := []storage.Book{
		{Title: "first", Author: "", Year: 0, Text: ""},
		{Title: "second", Author: "", Year: 1, Text: ""},
		{Title: "third", Author: "", Year: 2, Text: ""},
		{Title: "fourth", Author: "", Year: 3, Text: ""},
		{Title: "fifth", Author: "", Year: 4, Text: ""},
	}

	listLib := library.NewLibrary(storage.NewListStorage(), hash)

	for i := 0; i < len(books); i++ {
		err := listLib.AddBook(&books[i])
		if err != nil {
			panic(err)
		}
	}

	fmt.Println("Book titles:")
	tmp, _ := listLib.FindBookByTitle(&books[0].Title)
	fmt.Println(tmp.Title)
	tmp, _ = listLib.FindBookByTitle(&books[3].Title)
	fmt.Println(tmp.Title)

	mapLib := library.NewLibrary(storage.NewMapStorage(), hash2)

	for i := 0; i < len(books); i++ {
		err := mapLib.AddBook(&books[i])
		if err != nil {
			panic(err)
		}
	}

	tmp, _ = listLib.FindBookByTitle(&books[1].Title)
	fmt.Println(tmp.Title)
	tmp, _ = listLib.FindBookByTitle(&books[2].Title)
	fmt.Println(tmp.Title)
}
