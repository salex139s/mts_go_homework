package library

import "homework01/storage"

type Library struct {
	storage storage.Storage
	hash    func(*string) int
}

func NewLibrary(storage storage.Storage, hash func(*string) int) *Library {
	return &Library{storage: storage, hash: hash}
}

func (lib *Library) FindBookByTitle(title *string) (*storage.Book, error) {
	return lib.storage.GetBook(lib.hash(title))
}

func (lib *Library) AddBook(book *storage.Book) error {
	err := lib.storage.AddBook(lib.hash(&book.Title), book)
	return err
}
